#!/bin/bash
# Test to verify script CLI arguments
# Author: Naveen Akkapeddi <naveen.csci@gmail.com>

set -euxo pipefail

function test_cli {  
    echo "Testing mkdocs wrapper script..."
    if docker run -v "$(pwd)/test-project:/mkdocs_project" mkdocs:latest 123456; then
        echo "Command succeeded when it should have failed"
        exit 1
    fi
}

test_cli