#!/bin/bash
# Test to verify mkdocs serve functionality
# Author: Naveen Akkapeddi <naveen.csci@gmail.com>

set -euxo pipefail

TEST_FILE="site.tar.gz"
MKDOCS_URL="http://localhost:8000/"

function test_mkdocs_serve {  
    echo "Testing mkdocs serve..."
    docker run -d -ti -p 8000:8000 mkdocs:latest serve < site.tar.gz &
}

test_mkdocs_serve