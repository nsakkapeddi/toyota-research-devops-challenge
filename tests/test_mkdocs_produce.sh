#!/bin/bash
# Test to verify mkdocs produce functionality
# Author: Naveen Akkapeddi <naveen.csci@gmail.com>

set -euxo pipefail

TEST_FILE="site.tar.gz"

function test_mkdocs_produce {  
    echo "Testing mkdocs produce..."
    mkdocs new test-project
    docker run -v "$(pwd)/test-project:/mkdocs_project" mkdocs:latest produce > $TEST_FILE
}

test_mkdocs_produce