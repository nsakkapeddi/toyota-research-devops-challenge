FROM ubuntu:bionic

COPY mkdockerize.sh .

# Install mkdocs and its dependencies
RUN apt-get update && apt-get install -y python3 python3-pip 
RUN pip3 install mkdocs

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

ENTRYPOINT [ "./mkdockerize.sh" ]