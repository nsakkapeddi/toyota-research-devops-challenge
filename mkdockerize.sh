#!/bin/bash
# Wrapper script and entrypoint for Docker container that has MkDocs installed.
# Author: Naveen Akkapeddi <naveen.csci@gmail.com>

set -euxo pipefail

PATH_TO_DIR_INSIDE_CONTAINER="/mkdocs_project"

function produce {
    # This check makes sure that the local directory containing MkDocs project is mounted inside of the container
    if [ ! -d $PATH_TO_DIR_INSIDE_CONTAINER ] 
    then
        echo "Directory mkdocs_project does not exist, make sure to specify -v <local_dir>:/mkdocs_project in docker run" 
        exit 1
    fi
    cd $PATH_TO_DIR_INSIDE_CONTAINER
    mkdocs build #build the project
    tar czvf - $(pwd)
}

function serve {
    mkdir -p $PATH_TO_DIR_INSIDE_CONTAINER
    tar xvzf - -C /
    cd $PATH_TO_DIR_INSIDE_CONTAINER
    mkdocs serve -a 0.0.0.0:8000
}

subcommands=("produce" "serve")

if [[ ! " ${subcommands[@]} " =~ " $1 " ]]; then
    echo "Invalid subcommand provided: $1. Usage: $0 produce or $0 serve"
    exit 1
fi

$1