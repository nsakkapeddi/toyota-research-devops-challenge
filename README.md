# Toyota Research AD DevOps Challenge

This document provides a high level overview of how to run my DevOps coding challenge.
Thank you for taking the time in evaluating this project.

# Building the image
To build the docker image, run: `docker build -t img_name:desired_tag .`
This will build the docker image from the Dockerfile that exists in this directory.
It will also add an entrypoint script `mkdockerize.sh` that handles parsing arguments. 

* Example: `docker build -t mkdocs:latest .` 

# Using the image to build mkdocs
As specified in the challenge, the image can be run with `docker run docker_args img_name produce` that takes as input `mkdocs_dir`, a directory on your local filesystem containing an `MkDocs` project. This can the be piped to a file on your local filesystem, with the `>` operator

* Example: `docker run -v "/home/nakkapeddi/devel/mkdocs-test-site:/mkdocs_project" mkdocs:latest produce > site.tar.gz`

# Using the image to serve mkdocs project
As specified in the challenge, the image can be run with `docker run docker_args -p 8000:8000 <arguments> <the-docker-image-name> serve` to display the generated `MkDocs` project on port `8000`. 

* Example: `docker run -i -p 8000:8000 mkdocs:latest serve < site.tar.gz` 